import React, { Component } from 'react';
import {  View, StyleSheet } from 'react-native';
import { Constants } from 'expo';
import { createStackNavigator } from 'react-navigation';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
//import { Card } from 'react-native-elements'; // Version can be specified in package.json
import { Container, Header, Content, Form, Card, CardItem, Item, Input, Label,Button,Text,Icon } from 'native-base';
export default class Login extends Component {
  render() {
    return (
       <Container Style={styles.container}>
       <Content>
          <Form>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input />
            </Item>
            <Item floatingLabel last>
              <Label>Password</Label>
              <Input />
            </Item>
            <View style={styles.botonsRow}>
            <Button iconLeft rounded success>
            <Text>Signing</Text>
          </Button>
          <Button iconRight rounded danger>
            <Text>Cancel</Text>
          </Button>
            </View>
            
          </Form>
          <Text>Login with:</Text>
          <Text> </Text>
          <View style={styles.listItemClass}>
          <Button rounded style={{ backgroundColor: '#3B5998' }}>
              <Icon name="logo-facebook" />
            </Button>
             <Text> </Text>
            <Button rounded danger >
              <Icon name="logo-googleplus" />
            </Button>
            </View>
          </Content>
           </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
  },
  botonsRow:{
    flex:1,
    flexDirection: 'row',
    margin: 48,
    justifyContent: 'space-evenly'
  },
  listItemClass:{
    flex:1,
    flexDirection: 'column',
    marginLeft: 10,
    marginTop:-5,
    justifyContent: 'space-around'
  }
});
